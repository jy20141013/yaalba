import './assets/main.css'

import { createApp } from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import VueRouter from "vue-router";

Vue.use(ElementUI);
Vue.use(VueRouter)

createApp(App).mount('#app')
