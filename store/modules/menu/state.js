const state = () => ({
    globalMenu: [
        {
            parentName: 'HOME',
            menuLabel: [
                { id: 'DASH_BOARD', icon: 'el-icon-data-line', currentName: '대시보드', link: '/main', isShow: true},
                { id: 'MEMBER_ADD', icon: 'el-icon-user', currentName: '고객 관리', link: '/index', isShow: true },
                { id: 'MEMBER_EDIT', icon: 'el-icon-tickets', currentName: '주문 관리', link: '/', isShow: true },
                { id: 'MEMBER_LIST', icon: 'el-icon-suitcase', currentName: '상품 관리', link: '/customer/list', isShow: true },
                { id: 'MEMBER_DETAIL', icon: 'el-icon-edit', currentName: '게시물 관리', link: '/', isShow: true },
                { id: 'MEMBER_LOGOUT', icon: 'el-icon-box', currentName: '배송 관리', link: '/my-menu/logout', isShow: true },
                { id: 'MEMBER_LOGOUT', icon: 'el-icon-chat-dot-square', currentName: 'CS 관리', link: '/my-menu/logout', isShow: true },
            ]
        },
    ],
    selectedMenu: 'DASH_BOARD'
})

export default state
