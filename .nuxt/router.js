import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _0b8b3ad3 = () => interopDefault(import('../pages/detail.vue' /* webpackChunkName: "pages/detail" */))
const _2d769288 = () => interopDefault(import('../pages/event.vue' /* webpackChunkName: "pages/event" */))
const _7eb6f944 = () => interopDefault(import('../pages/goods.vue' /* webpackChunkName: "pages/goods" */))
const _75c5e052 = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _71ef728a = () => interopDefault(import('../pages/main.vue' /* webpackChunkName: "pages/main" */))
const _0d413ec8 = () => interopDefault(import('../pages/member.vue' /* webpackChunkName: "pages/member" */))
const _04afc44c = () => interopDefault(import('../pages/notice.vue' /* webpackChunkName: "pages/notice" */))
const _14402d3c = () => interopDefault(import('../pages/order.vue' /* webpackChunkName: "pages/order" */))
const _f2450318 = () => interopDefault(import('../pages/payment.vue' /* webpackChunkName: "pages/payment" */))
const _6acf1968 = () => interopDefault(import('../pages/question.vue' /* webpackChunkName: "pages/question" */))
const _f9a31dd8 = () => interopDefault(import('../pages/test.vue' /* webpackChunkName: "pages/test" */))
const _27442cc3 = () => interopDefault(import('../pages/customer/form.vue' /* webpackChunkName: "pages/customer/form" */))
const _5b3d539d = () => interopDefault(import('../pages/customer/list.vue' /* webpackChunkName: "pages/customer/list" */))
const _17881d51 = () => interopDefault(import('../pages/customer/list-filter-paging.vue' /* webpackChunkName: "pages/customer/list-filter-paging" */))
const _6543cbca = () => interopDefault(import('../pages/my-menu/logout.vue' /* webpackChunkName: "pages/my-menu/logout" */))
const _17b131bb = () => interopDefault(import('../pages/customer/detail/_id.vue' /* webpackChunkName: "pages/customer/detail/_id" */))
const _0ee1c798 = () => interopDefault(import('../pages/customer/edit/_id.vue' /* webpackChunkName: "pages/customer/edit/_id" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/detail",
    component: _0b8b3ad3,
    name: "detail"
  }, {
    path: "/event",
    component: _2d769288,
    name: "event"
  }, {
    path: "/goods",
    component: _7eb6f944,
    name: "goods"
  }, {
    path: "/login",
    component: _75c5e052,
    name: "login"
  }, {
    path: "/main",
    component: _71ef728a,
    name: "main"
  }, {
    path: "/member",
    component: _0d413ec8,
    name: "member"
  }, {
    path: "/notice",
    component: _04afc44c,
    name: "notice"
  }, {
    path: "/order",
    component: _14402d3c,
    name: "order"
  }, {
    path: "/payment",
    component: _f2450318,
    name: "payment"
  }, {
    path: "/question",
    component: _6acf1968,
    name: "question"
  }, {
    path: "/test",
    component: _f9a31dd8,
    name: "test"
  }, {
    path: "/customer/form",
    component: _27442cc3,
    name: "customer-form"
  }, {
    path: "/customer/list",
    component: _5b3d539d,
    name: "customer-list"
  }, {
    path: "/customer/list-filter-paging",
    component: _17881d51,
    name: "customer-list-filter-paging"
  }, {
    path: "/my-menu/logout",
    component: _6543cbca,
    name: "my-menu-logout"
  }, {
    path: "/customer/detail/:id?",
    component: _17b131bb,
    name: "customer-detail-id"
  }, {
    path: "/customer/edit/:id?",
    component: _0ee1c798,
    name: "customer-edit-id"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
