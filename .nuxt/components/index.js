export { default as CommonLoadingIcon } from '../../components/common/loading-icon.vue'
export { default as LayoutAdminHeader } from '../../components/layout/admin-header.vue'
export { default as LayoutAdminList } from '../../components/layout/admin-list.vue'
export { default as LayoutAdminSidebarAvatar } from '../../components/layout/admin-sidebar-avatar.vue'
export { default as LayoutAdminSidebarMenu } from '../../components/layout/admin-sidebar-menu.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
